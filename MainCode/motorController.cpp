#include "Arduino.h"
#include "motorController.h"



motorController::motorController(Motor m1, Motor m2, Motor m3, Motor m4) {
  _M1 = m1;
  _M2 = m2;
  _M3 = m3;
  _M4 = m4;
}
void motorController::forwards() {
  _M1.adjustableSpeedFord(255); //176
  _M2.adjustableSpeedFord(255); //188
  _M3.adjustableSpeedFord(255); //120
  _M4.adjustableSpeedFord(255); //191
}
void motorController::backwards() {
  _M1.adjustableSpeedBack(176 / 2);
  _M2.adjustableSpeedBack(188 / 2);
  _M3.adjustableSpeedBack(120 / 2);
  _M4.adjustableSpeedBack(191 / 2);
}
void motorController::rotateRight() {
  _M1.adjustableSpeedFord(176 / 3);
  _M2.adjustableSpeedBack(188 / 3);
  _M3.adjustableSpeedFord(120 / 3);
  _M4.adjustableSpeedBack(191 / 3);
}
void motorController::rotateLeft() {
  _M1.adjustableSpeedBack(176 / 4);
  _M2.adjustableSpeedFord(188 / 4);
  _M3.adjustableSpeedBack(120 / 4);
  _M4.adjustableSpeedFord(191 / 4);
}
void motorController::left() {
  _M1.adjustableSpeedBack(206 );
  _M2.adjustableSpeedFord(188 );
  _M3.adjustableSpeedFord(120 );
  _M4.adjustableSpeedBack(211 );
}
void motorController::right() {
  _M1.adjustableSpeedFord(206 ); //156
  _M2.adjustableSpeedBack(188 ); //228
  _M3.adjustableSpeedBack(120 ); //100
  _M4.adjustableSpeedFord(191 ); //171
}
void motorController::diagonalRightUp() {
  _M1.adjustableSpeedFord(100);
  _M4.adjustableSpeedFord(100);
}
void motorController::diagonalLeftUp() {
  _M2.adjustableSpeedFord(100);
  _M3.adjustableSpeedFord(100);
}

void motorController::diagonalLeftDown() {
  _M1.adjustableSpeedBack(100);
  _M4.adjustableSpeedBack(100);
}
void motorController::diagonalRightDown() {
  _M2.adjustableSpeedBack(100);
  _M3.adjustableSpeedBack(100);
}

void motorController::haults() {
  _M1.hault();
  _M2.hault();
  _M3.hault();
  _M4.hault();
}
void motorController::haulAss(int newSpeed) {
  _M1.adjustableSpeedFord(newSpeed);
  _M2.adjustableSpeedFord(newSpeed);
  _M3.adjustableSpeedFord(newSpeed);
  _M4.adjustableSpeedFord(newSpeed);
}

void motorController::forwardsAdj() {
  _M1.adjustableSpeedFord(176 / 2);
  _M2.adjustableSpeedFord(188 / 2);
  _M3.adjustableSpeedFord(120 / 2);
  _M4.adjustableSpeedFord(191 / 2);
}


void motorController::backwardsAdj() {
  _M1.adjustableSpeedBack(176/3);
  _M2.adjustableSpeedBack(188/3);
  _M3.adjustableSpeedBack(120/3);
  _M4.adjustableSpeedBack(191/3);
}

void motorController::leftAdj() {
  _M1.adjustableSpeedBack(206/3);
  _M2.adjustableSpeedFord(188/3);
  _M3.adjustableSpeedFord(120/3);
  _M4.adjustableSpeedBack(211/3);
}

void motorController::rightAdj() {
  _M1.adjustableSpeedFord(156/3);
  _M2.adjustableSpeedBack(228/3);
  _M3.adjustableSpeedBack(100/3);
  _M4.adjustableSpeedFord(171/3);
}

void motorController::backwardsUp(){
  _M1.adjustableSpeedBack(200);
  _M2.adjustableSpeedBack(200);
  _M3.adjustableSpeedBack(200);
  _M4.adjustableSpeedBack(200);
}



